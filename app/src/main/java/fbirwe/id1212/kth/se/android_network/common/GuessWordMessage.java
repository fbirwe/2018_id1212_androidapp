/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fbirwe.id1212.kth.se.android_network.common;

/**
 *
 * @author fredericbirwe
 */
public class GuessWordMessage extends Message {
    private String word;
   
    public GuessWordMessage(String word) {
        super(MsgType.GUESS_WORD);
        this.word = word;        
    }
    
    public GuessWordMessage( String[] attributes ) {
        this( attributes[1] );
    }
    
    public String getWord() {
        return this.word;
    }
    
    @Override
    public String toMessageString() {
        return super.toMessageString() + this.word + Constants.ATTRIBUTE_DELIMETER;
    }
}

