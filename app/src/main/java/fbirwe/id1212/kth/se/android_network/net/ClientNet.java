package fbirwe.id1212.kth.se.android_network.net;

import android.util.Log;
import fbirwe.id1212.kth.se.android_network.common.Constants;
import fbirwe.id1212.kth.se.android_network.common.DisconnectMessage;
import fbirwe.id1212.kth.se.android_network.common.Helper;
import fbirwe.id1212.kth.se.android_network.common.Message;
import fbirwe.id1212.kth.se.android_network.common.WrongMessageSizeException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.Executor;
import java.util.concurrent.ForkJoinPool;

public class ClientNet implements Runnable {
    private static final String FATAL_COMMUNICATION_MSG = "Lost connection.";
    private static final String FATAL_DISCONNECT_MSG = "Could not disconnect, will leave ungracefully.";

    private final ByteBuffer msgFromServer = ByteBuffer.allocateDirect(Constants.MAX_MSG_LENGTH);
    private final Queue<ByteBuffer> messagesToSend = new ArrayDeque<>();
    private final List<CommunicationListener> listeners = new ArrayList<>();
    private InetSocketAddress serverAddress;
    private String host;
    private int port;

    private SocketChannel socketChannel;
    private Selector selector;
    private boolean connected;
    private volatile boolean timeToSend = false;

    @Override
    public void run() {
        try {
            serverAddress = new InetSocketAddress(InetAddress.getByName(host), port);

            initConnection();
            initSelector();

            while (connected || !messagesToSend.isEmpty()) {
                if (timeToSend) {
                    socketChannel.keyFor(selector).interestOps(SelectionKey.OP_WRITE);
                    timeToSend = false;
                }

                selector.select();
                for (SelectionKey key : selector.selectedKeys()) {
                    selector.selectedKeys().remove(key);
                    if (!key.isValid()) {
                        continue;
                    }
                    if (key.isConnectable()) {
                        completeConnection(key);
                    } else if (key.isReadable()) {

                        receiveFromServer(key);
                    } else if (key.isWritable()) {
                        sendToServer(key);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(FATAL_COMMUNICATION_MSG);
        }
        try {
            doDisconnect();
        } catch (IOException ex) {
            System.err.println(FATAL_DISCONNECT_MSG);
        }
    }

    public void addCommunicationListener(CommunicationListener listener) {
        listeners.add(listener);
    }

    public void connect(String host, int port) {
        this.host = host;
        this.port = port;
        new Thread(this).start();
    }

    private void initSelector() throws IOException {
        selector = Selector.open();
        socketChannel.register(selector, SelectionKey.OP_CONNECT);
    }

    private void initConnection() throws IOException {
        socketChannel = SocketChannel.open();
        socketChannel.configureBlocking(false);
        socketChannel.connect(serverAddress);
        connected = true;
    }

    private void completeConnection(SelectionKey key) throws IOException {
        socketChannel.finishConnect();
        key.interestOps(SelectionKey.OP_READ);
        try {
            InetSocketAddress remoteAddress = (InetSocketAddress) socketChannel.getRemoteAddress();
            notifyConnectionDone(remoteAddress);
        } catch (IOException couldNotGetRemAddrUsingDefaultInstead) {
            notifyConnectionDone(serverAddress);
        }
    }

    public void disconnect() throws IOException {
        connected = false;
        sendMessage( new DisconnectMessage() );
    }

    private void doDisconnect() throws IOException {
        socketChannel.close();
        socketChannel.keyFor(selector).cancel();
        notifyDisconnectionDone();
    }


    public void sendMessage( Message msg ) {

        String msgString = Helper.messageToString(msg);

        synchronized (messagesToSend) {
            messagesToSend.add(ByteBuffer.wrap(msgString.getBytes()));
        }
        timeToSend = true;
        selector.wakeup();
    }

    private void sendToServer(SelectionKey key) throws IOException {
        ByteBuffer msg;
        synchronized (messagesToSend) {
            while ((msg = messagesToSend.peek()) != null) {
                socketChannel.write(msg);
                if (msg.hasRemaining()) {
                    return;
                }
                messagesToSend.remove();
            }
            key.interestOps(SelectionKey.OP_READ);
        }
    }

    private void receiveFromServer(SelectionKey key) throws IOException {
        msgFromServer.clear();
        int numOfReadBytes = socketChannel.read(msgFromServer);
        if (numOfReadBytes == -1) {
            throw new IOException(FATAL_COMMUNICATION_MSG);
        }

        Message receivedMessage;
        try {
            receivedMessage = extractMessageFromBuffer();
            notifyMsgReceived( receivedMessage );

        } catch (WrongMessageSizeException ex) {
        }
    }

    private Message extractMessageFromBuffer() throws WrongMessageSizeException {
        msgFromServer.flip();
        byte[] bytes = new byte[msgFromServer.remaining()];
        msgFromServer.get(bytes);

        return Helper.stringToMessage(new String(bytes));
    }

    private void notifyConnectionDone(final InetSocketAddress connectedAddress) {
        for (final CommunicationListener listener : listeners) {
            listener.connected( connectedAddress );
            /*pool.execute(new Runnable() {
                @Override
                public void run() {
                    Looper.prepare();
                    listener.connected(connectedAddress);
                }
            });*/
        }
    }

    private void notifyDisconnectionDone() {
        for (final CommunicationListener listener : listeners) {
            listener.disconnected();
            /*pool.execute(new Runnable() {
                @Override
                public void run() {
                    Looper.prepare();
                    listener.disconnected();
                }
            });*/
        }
    }

    private void notifyMsgReceived(final Message msg) {
        for (final CommunicationListener listener : listeners) {
            Log.i("message received: ", msg.toString() );
            listener.recvdMsg(msg);
        }
    }

}
