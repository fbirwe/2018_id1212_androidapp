/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fbirwe.id1212.kth.se.android_network.common;

import java.util.ArrayList;
import java.util.StringJoiner;

/**
 *
 * @author fredericbirwe
 */
public class StatusMessage extends Message{
    
    private ArrayList<Character> correctGuessed;
    private String word;
    private int remainingAttempts;
    
    public StatusMessage( String[] attributes ) {
        super(MsgType.STATUS);
        
        ArrayList<Character> correctGuessed = new ArrayList<>();
        for(int i = 0; i < attributes[1].length(); i++) {
            correctGuessed.add( attributes[1].charAt(i) );
        }
        
        this.correctGuessed = correctGuessed;
        this.word = attributes[2];
        this.remainingAttempts = Integer.parseInt(attributes[3]);
    }
    
    public ArrayList<Character> getCorrectGuessed() {
        return this.correctGuessed;
    }
    
    public int getRemainingAttempts() {
        return this.remainingAttempts;
    }
    
    public String getWord() {
        return this.word;
    }
    
    @Override
    public String toMessageString() {
        StringJoiner joiner = new StringJoiner( Constants.ATTRIBUTE_DELIMETER);
        joiner.add(this.correctGuessed.toString());
        joiner.add(this.word);
        joiner.add(this.remainingAttempts + "");
        
        return super.toMessageString() + joiner.toString();
    }
    
}
