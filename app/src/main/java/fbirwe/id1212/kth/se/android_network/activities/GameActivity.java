package fbirwe.id1212.kth.se.android_network.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import fbirwe.id1212.kth.se.android_network.R;
import fbirwe.id1212.kth.se.android_network.adapter.KeyboardAdapter;
import fbirwe.id1212.kth.se.android_network.common.Constants;
import fbirwe.id1212.kth.se.android_network.common.DisconnectMessage;
import fbirwe.id1212.kth.se.android_network.common.GameEndMessage;
import fbirwe.id1212.kth.se.android_network.common.GuessWordMessage;
import fbirwe.id1212.kth.se.android_network.common.Message;
import fbirwe.id1212.kth.se.android_network.common.NewGameMessage;
import fbirwe.id1212.kth.se.android_network.common.ResetMessage;
import fbirwe.id1212.kth.se.android_network.common.StatusMessage;
import fbirwe.id1212.kth.se.android_network.firebase.MyFirebaseMessagingService;
import fbirwe.id1212.kth.se.android_network.net.ClientNet;
import fbirwe.id1212.kth.se.android_network.net.CommunicationListener;
import java.net.InetSocketAddress;
import java.util.StringJoiner;

public class GameActivity extends AppCompatActivity {
    private ClientNet serverConnection;
    private Handler handler;
    private KeyboardAdapter adapter;
    private final int id = 10101010;
    private int totalAttempts;
    private ImageView[] hangman;
    private BroadcastReceiver receiver;

    @Override
    public void onStart() {
        super.onStart();
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    String title = intent.getStringExtra("title");
                    String body = intent.getStringExtra("body");

                    showNotificationDialog( title, body );

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        };

        LocalBroadcastManager.getInstance( GameActivity.this ).registerReceiver((receiver),
            new IntentFilter(MyFirebaseMessagingService.REQUEST_ACCEPT)
        );
    }



    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance( GameActivity.this ).unregisterReceiver(receiver);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        handler = new Handler( getApplicationContext().getMainLooper() );

        if ( isNetworkAvailable() ) {
            serverConnection = new ClientNet();
            serverConnection.connect(Constants.IP, Constants.PORT);
            serverConnection.addCommunicationListener(new CommunicationHandler());
        } else {
            System.out.println("here");
        }

        // set score
        setScore(0);
    }

    private void initGameUI( NewGameMessage msg ) {
        adapter = new KeyboardAdapter(this, 0, serverConnection);

        final String[] alphabet = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};

        for( String letter : alphabet ) {
            adapter.add( letter );
        }

        totalAttempts = msg.getRemainingAttempts();


        final GridView keyboard = (GridView) findViewById( R.id.keyboard_view );
        keyboard.setNumColumns(9);
        keyboard.setTranscriptMode( GridView.TRANSCRIPT_MODE_DISABLED );
        keyboard.setAdapter( adapter );

        // set guess word
        Button guessWordButton = (Button) findViewById( R.id.guess_word_button );

        guessWordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String guessedWord = ((EditText) findViewById( R.id.guess_word_editText )).getText().toString();

                if( guessedWord.length() > 0 ) {
                    serverConnection.sendMessage(new GuessWordMessage( guessedWord ));
                }
            }
        });

        // set hangman visualization
        hangman = new ImageView[] {
            (ImageView) findViewById(R.id.hangman_01),
            (ImageView) findViewById(R.id.hangman_02),
            (ImageView) findViewById(R.id.hangman_03),
            (ImageView) findViewById(R.id.hangman_04),
            (ImageView) findViewById(R.id.hangman_05),
            (ImageView) findViewById(R.id.hangman_06),
            (ImageView) findViewById(R.id.hangman_07)
        };

        for( ImageView hangmanPart : hangman ) {
            hangmanPart.setVisibility( View.INVISIBLE );
        }

        // set remaining attempts
        final TextView remainingAttempts = (TextView) findViewById( R.id.remaining_attempts );
        remainingAttempts.setText( "" + msg.getRemainingAttempts() );

        // set solution word
        final TextView solutionWord = (TextView) findViewById( R.id.solution_word );
        StringBuilder solution = new StringBuilder();
        for( int i = 0; i < msg.getWordLength(); i++ ) {
            solution.append("_");
            solution.append(" ");
        }
        solutionWord.setText( solution.toString() );
    }

    private void updateGameUI( StatusMessage msg ) {
        // set remaining attempts
        final TextView remainingAttempts = (TextView) findViewById( R.id.remaining_attempts );
        remainingAttempts.setText( "" + msg.getRemainingAttempts() );

        // set solution word
        final TextView solutionWord = (TextView) findViewById( R.id.solution_word );
        StringBuilder solution = new StringBuilder();
        for( int i = 0; i < msg.getWord().length(); i++ ) {
            if( msg.getCorrectGuessed().contains( msg.getWord().charAt(i) ) ) {
                solution.append(msg.getWord().charAt(i));
            } else {
                solution.append('_');
            }

            solution.append(' ');

        }

        solutionWord.setText( solution.toString() );

        // set Hangman
        setHangman( msg.getRemainingAttempts() );
    }

    private void setScore( int newScore ) {
        final TextView score = (TextView) findViewById( R.id.score );
        score.setText("" + newScore );
    }

    private void setHangman( int remainingAttempts ) {
        double mistakeValue = ((1.0 - ( (double)remainingAttempts / (double)totalAttempts )) * (double) hangman.length);

        for (int i = 0; i <= mistakeValue - 1.0; i++ ) {
            if( hangman[i].getVisibility() == View.INVISIBLE ) {
                Log.i( "attempt", "" + (mistakeValue) );

                hangman[i].setVisibility( View.VISIBLE );
                Animation startAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
                hangman[i].startAnimation(startAnimation);
            }
        }
    }

    private void showGameEndDialog(boolean won, GameEndMessage msg ) {
        // 1. Instantiate an <code><a href="/reference/android/app/AlertDialog.Builder.html">AlertDialog.Builder</a></code> with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder( GameActivity.this, R.style.DialogTheme );

        // 2. Chain together various setter methods to set the dialog characteristics
        StringJoiner message = new StringJoiner("\n");

        if( won ) {
            message.add( getString( R.string.game_won_message ) );

            builder.setTitle( R.string.game_won_title );
        } else {
            message.add( getString( R.string.game_lost_message ) );

            builder.setTitle( R.string.game_won_title );
        }

        message.add( getString( R.string.retry_message ) );
        message.add( "your score is: " + msg.getScore() );
        message.add("the word we were looking for was\"" + msg.getWord() + "\"");

        builder.setMessage( message.toString() );

        builder.setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                serverConnection.sendMessage( new ResetMessage() );
            }
        });
        builder.setNegativeButton(R.string.quit, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                serverConnection.sendMessage( new DisconnectMessage() );
                Intent intent = new Intent( GameActivity.this , WelcomeActivity.class );
                startActivity( intent );
            }
        });

        // 3. Get the <code><a href="/reference/android/app/AlertDialog.html">AlertDialog</a></code> from <code><a href="/reference/android/app/AlertDialog.Builder.html#create()">create()</a></code>
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);

        dialog.show();
    }

    private void showNotificationDialog( String title, String body ) {
        AlertDialog.Builder builder = new AlertDialog.Builder( GameActivity.this, R.style.DialogTheme );

        builder.setMessage( body );
        builder.setTitle( title );

        builder.setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // Implementation
            }
        });
        builder.setNegativeButton(R.string.quit, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // Implementation
            }
        });

        // 3. Get the <code><a href="/reference/android/app/AlertDialog.html">AlertDialog</a></code> from <code><a href="/reference/android/app/AlertDialog.Builder.html#create()">create()</a></code>
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);

        dialog.show();
    }

    private class CommunicationHandler implements CommunicationListener {

        @Override
        public void recvdMsg(final Message msg) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    switch ( msg.getType() ) {
                        case NEW_GAME :
                            NewGameMessage newGameMessage = (NewGameMessage) msg;
                            initGameUI( newGameMessage );
                            break;
                        case STATUS :
                            StatusMessage statusMessage = (StatusMessage) msg;
                            updateGameUI( statusMessage );
                            break;
                        case GAME_LOST :
                            GameEndMessage gameLostMessage = (GameEndMessage) msg;

                            showGameEndDialog( false, gameLostMessage );
                            setScore( gameLostMessage.getScore() );
                            break;
                        case GAME_WON :
                            GameEndMessage gameWonMessage = (GameEndMessage) msg;

                            showGameEndDialog( true, gameWonMessage );
                            setScore( gameWonMessage.getScore() );
                            break;
                    }
                }
            });
        }

        @Override
        public void connected(InetSocketAddress serverAddress) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText( GameActivity.this , "connected", Toast.LENGTH_LONG);
                }
            });
        }

        @Override
        public void disconnected() {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText( GameActivity.this , "disconnect", Toast.LENGTH_LONG);
                    Intent intent = new Intent( GameActivity.this , WelcomeActivity.class );
                    startActivity( intent );
                }
            });
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager)
            getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        // if no network is available networkInfo will be null
        // otherwise check if we are connected
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }}
