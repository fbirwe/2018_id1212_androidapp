package fbirwe.id1212.kth.se.android_network.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import fbirwe.id1212.kth.se.android_network.R;
import fbirwe.id1212.kth.se.android_network.common.GuessCharMessage;
import fbirwe.id1212.kth.se.android_network.common.Message;
import fbirwe.id1212.kth.se.android_network.net.ClientNet;

public class KeyboardAdapter extends ArrayAdapter {
    private ClientNet clientNet;

    public KeyboardAdapter(@NonNull Context context, int resource, ClientNet clientNet ) {
        super(context, resource);
        this.clientNet = clientNet;
    }

    private class ViewHolder {
        Button button;
    }

    public View getView(int position, View row, ViewGroup parent) {
        ViewHolder holder = null;

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        row = inflater.inflate( R.layout.keyboard_button, null );

        holder = new ViewHolder();
        holder.button = row.findViewById( R.id.keyboard_button );
        holder.button.setBackgroundColor( getContext().getColor( R.color.colorPrimaryBright ) );
        holder.button.setText( getItem(position).toString() );
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("received the following", (String)((Button) v).getText());
                String guessedChar = (String)((Button) v).getText();
                clientNet.sendMessage(new GuessCharMessage( guessedChar.charAt(0) ));

                Button button = (Button) v;
                button.setEnabled(false);
                button.setBackgroundColor( getContext().getColor(R.color.colorPrimaryDark) );
            }
        });

        return row;
    }
}
