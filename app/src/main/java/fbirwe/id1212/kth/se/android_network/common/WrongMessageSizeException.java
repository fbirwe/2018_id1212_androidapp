/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fbirwe.id1212.kth.se.android_network.common;

/**
 *
 * @author fredericbirwe
 */
public class WrongMessageSizeException extends Exception {

    public WrongMessageSizeException() {
        super("The received message does not have the expected size");
    }
    
}
