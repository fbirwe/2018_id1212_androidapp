/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fbirwe.id1212.kth.se.android_network.common;

/**
 *
 * @author fredericbirwe
 */
public class DisconnectMessage extends Message implements MessageString{
    
    public DisconnectMessage() {
        super(MsgType.DISCONNECT);        
    }
    
}
