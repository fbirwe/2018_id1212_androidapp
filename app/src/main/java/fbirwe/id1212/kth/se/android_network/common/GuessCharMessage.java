/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fbirwe.id1212.kth.se.android_network.common;

/**
 *
 * @author fredericbirwe
 */
public class GuessCharMessage extends Message {
    private char c;
    
    public GuessCharMessage(char c) {
        super(MsgType.GUESS_CHAR);
        this.c = c;        
    }
    
    public GuessCharMessage( String[] attributes ) {
        this( attributes[1].charAt(0) );
    }
    
    public char getChar() {
        return this.c;
    }
    
    @Override
    public String toMessageString() {
        return super.toMessageString() + this.c + Constants.ATTRIBUTE_DELIMETER;
    }
}
