package fbirwe.id1212.kth.se.android_network.net;

import fbirwe.id1212.kth.se.android_network.common.Message;
import java.net.InetSocketAddress;

public interface CommunicationListener {

    public void recvdMsg(Message msg);

    public void connected(InetSocketAddress serverAddress);

    public void disconnected();
}
